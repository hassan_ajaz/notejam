#!/bin/sh

cd /application/notejam
python manage.py syncdb --noinput
python manage.py migrate
python manage.py runserver 0.0.0.0:8000
