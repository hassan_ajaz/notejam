provider "aws" {
  region = "eu-west-1"
}

resource "aws_ecr_repository" "notejam" {
  name                 = "notejam"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }
}

resource "aws_lb" "notejam" {
  name               = "notejam"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.notejam-lb.id}"]
  subnets            = ["subnet-3e715b58", "subnet-adb389e5", "subnet-be8df9e4"]
}

resource "aws_lb_target_group" "notejam" {
  name        = "notejam-lb-tg"
  port        = 8000
  protocol    = "HTTP"
  vpc_id      = "vpc-2ad52c53"
  target_type = "ip"
}

resource "aws_lb_listener" "notejam" {
  load_balancer_arn = "${aws_lb.notejam.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.notejam.arn}"
  }
}

resource "aws_ecs_service" "notejam" {
  name            = "notejam"
  cluster         = "${aws_ecs_cluster.notejam.id}"
  task_definition = "${aws_ecs_task_definition.notejam.arn}"
  launch_type     = "FARGATE"
  desired_count   = 3

  load_balancer {
    target_group_arn = "${aws_lb_target_group.notejam.arn}"
    container_name   = "notejam"
    container_port   = 8000
  }

  network_configuration {
    assign_public_ip = true
    security_groups  = ["${aws_security_group.notejam-app.id}"]
    subnets          = ["subnet-3e715b58", "subnet-adb389e5", "subnet-be8df9e4"]
  }
}

resource "aws_ecs_task_definition" "notejam" {
  family                   = "notejam"
  container_definitions    = file("../container.json")
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  network_mode             = "awsvpc"
  execution_role_arn       = "arn:aws:iam::384525574740:role/ecsTaskExecutionRole"
  task_role_arn            = "arn:aws:iam::384525574740:role/ecsTaskExecutionRole"
}

data "aws_iam_role" "ecs_task_execution_role" {
  name = "ecsTaskExecutionRole"
}


resource "aws_ecs_cluster" "notejam" {
  name = "notejam"
}

resource "aws_db_instance" "notejam" {
  allocated_storage = 20
  storage_type      = "gp2"
  engine            = "postgres"
  engine_version    = "11.8"
  instance_class    = "db.t2.micro"
  name              = "notejam"
  username          = "postgres"
  password          = "postgres123"
}

resource "aws_security_group" "notejam-lb" {
  name   = "notejam-lb-sg"
  vpc_id = "vpc-2ad52c53"

  ingress {
    from_port   = 80
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "notejam-app" {
  name   = "notejam-app-sg"
  vpc_id = "vpc-2ad52c53"

  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
