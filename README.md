# Notejam on AWS Elastic Container Service

The Notejam Django Application hosted in the AWS Cloud.

- Dockerised the Django Notejam application
- Created an AWS Infrastructure in Terraform
- Defined a CI/CD pipeline using Gitlab
